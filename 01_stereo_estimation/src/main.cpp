#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include <fstream>
#include "main.h"

// start with the following parameters: data/im2.png data/im6.png output/cones
int main(int argc, char **argv) {

  ////////////////
  // Parameters //
  ////////////////

  // camera setup parameters
  const double focal_length = 1247;
  const double baseline = 213;

  // stereo estimation parameters
  int dmin = 67;
  if (argc > 4) dmin = std::stoi(argv[4]);
  int window_size = 5;
  if (argc > 5) window_size = std::stoi(argv[5]);
  double weight = 300;
  if (argc > 6) weight = std::stod(argv[6]);
  double scale = 3;
  if (argc > 7) scale = std::stod(argv[7]);

  ///////////////////////////
  // Commandline arguments //
  ///////////////////////////

  if (argc < 4) {
    std::cerr << "Usage: " << argv[0] << " IMAGE1 IMAGE2 OUTPUT_FILE [dmin] [window_size] [weight] [scale]"
              << std::endl;
    return 1;
  }

  cv::Mat image1 = cv::imread(argv[1], cv::IMREAD_GRAYSCALE);
  cv::Mat image2 = cv::imread(argv[2], cv::IMREAD_GRAYSCALE);
  const std::string output_file = argv[3];

  if (!image1.data) {
    std::cerr << "No image1 data" << std::endl;
    return EXIT_FAILURE;
  }

  if (!image2.data) {
    std::cerr << "No image2 data" << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "------------------ Parameters -------------------" << std::endl;
  std::cout << "focal_length = " << focal_length << std::endl;
  std::cout << "baseline = " << baseline << std::endl;
  std::cout << "window_size = " << window_size << std::endl;
  std::cout << "occlusion weights = " << weight << std::endl;
  std::cout << "disparity added due to image cropping = " << dmin << std::endl;
  std::cout << "scaling of disparity images to show = " << scale << std::endl;
  std::cout << "output filename = " << argv[3] << std::endl;
  std::cout << "-------------------------------------------------" << std::endl;

  int height = image1.size().height;
  int width = image1.size().width;

  ////////////////////
  // Reconstruction //
  ////////////////////

  // Naive disparity image
  //cv::Mat naive_disparities = cv::Mat::zeros(height - window_size, width - window_size, CV_8UC1);
  cv::Mat naive_disparities = cv::Mat::zeros(height, width, CV_8UC1);
  cv::Mat dp_disparities = cv::Mat::zeros(height, width, CV_8UC1);

  StereoEstimation_Naive(
      window_size, dmin,
      image1, image2,
      naive_disparities, scale);
  StereoEstimation_DP(
      weight,
      image1, image2,
      dp_disparities, scale);

  ////////////
  // Output //
  ////////////

  // reconstruction
  Disparity2PointCloud(
      output_file + "_naive",
      height, width, naive_disparities,
      window_size, dmin, baseline, focal_length);

  // save / display images
  std::stringstream out1;
  out1 << output_file << "_naive.png";
  cv::imwrite(out1.str(), naive_disparities);

  cv::namedWindow("Naive", cv::WINDOW_AUTOSIZE);
  cv::imshow("Naive", naive_disparities);

  // reconstruction
  Disparity2PointCloud(
      output_file + "_dp",
      height, width, dp_disparities,
      window_size, dmin, baseline, focal_length);

  // save / display images
  std::stringstream out2;
  out2 << output_file << "_dp.png";
  cv::imwrite(out2.str(), dp_disparities);

  cv::namedWindow("DP", cv::WINDOW_AUTOSIZE);
  cv::imshow("DP", dp_disparities);

  cv::waitKey(0);

  return 0;
}

void StereoEstimation_Naive(
    const int &window_size,
    const int &dmin,
    cv::Mat &image1, cv::Mat &image2,
    cv::Mat &naive_disparities, const double &scale) {
  int height = image1.size().height;
  int width = image1.size().width;
  int half_window_size = window_size / 2;

  for (int i = half_window_size; i < height - half_window_size; ++i) {

    std::cout
        << "Calculating disparities for the naive approach... "
        << std::ceil(((i - half_window_size + 1) / static_cast<double>(height - window_size + 1)) * 100) << "%\r"
        << std::flush;

    for (int j = half_window_size; j < width - half_window_size; ++j) {
      int min_ssd = INT_MAX;
      int disparity = 0;

      for (int d = -j + half_window_size; d < width - j - half_window_size; ++d) {
        int ssd = 0;

        for (int k = -half_window_size + i; k <= half_window_size + i; ++k) {
          for (int l = -half_window_size + j; l <= half_window_size + j; ++l) {
            int diff = image1.at<uchar>(k, l) - image2.at<uchar>(k, l + d);
            ssd += diff * diff;
          }
        }

        if (ssd < min_ssd) {
          min_ssd = ssd;
          disparity = d;
        }
      }

      naive_disparities.at<uchar>(i - half_window_size, j - half_window_size) = std::abs(disparity) * scale;
    }
  }

  std::cout << "Calculating disparities for the naive approach... Done.\r" << std::flush;
  std::cout << std::endl;
}

void StereoEstimation_DP(
    const double &lambda,
    cv::Mat &image1, cv::Mat &image2,
    cv::Mat &dp_disparities, const double &scale) {
  cv::Mat M;
  cv::Mat C;
  int rows = image1.rows;
  int cols = image1.cols;
  for (int row = 0; row < rows; ++row) {

    std::cout
        << "Calculating disparities for the DP approach... "
        << std::ceil(((row + 1) / static_cast<double>(rows + 1)) * 100) << "%\r"
        << std::flush;

    M = cv::Mat::zeros(cols, cols, CV_8UC1);
    C = cv::Mat::zeros(cols, cols, CV_64FC1);
    for (int i = 0; i < cols; i++) {
      C.at<double>(i, 0) = i * lambda;
      C.at<double>(0, i) = i * lambda;
    }

    for (int i = 1; i < cols; ++i) {
      for (int j = 1; j < cols; ++j) {

        uchar diff = image1.at<uchar>(row, i) - image2.at<uchar>(row, j);
        double match = C.at<double>(i - 1, j - 1) + diff * diff;
        double leftOcclusion = C.at<double>(i - 1, j) + lambda;
        double rightOcclusion = C.at<double>(i, j - 1) + lambda;

        double min = std::min(std::min(match, leftOcclusion), rightOcclusion);
        if (min == match) M.at<uchar>(i, j) = 1;
        else if (min == leftOcclusion) M.at<uchar>(i, j) = 2;
        else if (min == rightOcclusion) M.at<uchar>(i, j) = 3;

        C.at<double>(i, j) = min;
      }
    }

    int i = cols - 1, j = cols - 1;
    while (i > 0 && j > 0) {
      switch (M.at<uchar>(i, j)) {
        case 1: // match
          i--;
          j--;
          break;
        case 2: // occluded from left
          i--;
          break;
        case 3: // occluded from right
          j--;
          break;
      }
      dp_disparities.at<uchar>(row, i) = abs(i - j) * scale;
    }
  }

  std::cout << "Calculating disparities for the DP approach... Done.\r" << std::flush;
  std::cout << std::endl;
}

void Disparity2PointCloud(
    const std::string &output_file,
    int height, int width, cv::Mat &disparities,
    const int &window_size,
    const int &dmin, const double &baseline, const double &focal_length) {
  std::stringstream out3d;
  out3d << output_file << ".xyz";
  std::ofstream outfile(out3d.str());
  for (int i = 0; i < height - window_size; ++i) {
    std::cout << "Reconstructing 3D point cloud from disparities... "
              << std::ceil(((i) / static_cast<double>(height - window_size + 1)) * 100) << "%\r" << std::flush;
    for (int j = 0; j < width - window_size; ++j) {
      if (disparities.at<uchar>(i, j) == 0) continue;

      const double t = baseline / ((disparities.at<uchar>(i, j) / 6.0) + 200);
      const double Z = t * focal_length;
      const double X = t * j;
      const double Y = t * i;

      outfile << X << " " << Y << " " << Z << std::endl;
    }
  }

  std::cout << "Reconstructing 3D point cloud from disparities... Done.\r" << std::flush;
  std::cout << std::endl;
}
