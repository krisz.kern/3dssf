#pragma once

cv::Mat upsample(cv::Mat &src, cv::Mat &dst, cv::Mat &dispSrc,
                 int uf, int d, double sigmaS, double sigmaR);

void jointBilateralFilter(cv::Mat &src, cv::Mat &dst,
                          cv::Mat &dispSrc, cv::Mat &dispDst,
                          int d, double sigmaS, double sigmaR);

double distance(int x1, int y1, int x2, int y2);

double gaussian(double x, double sigma);

cv::Vec3f gaussian(cv::Vec3f x, double sigma);
