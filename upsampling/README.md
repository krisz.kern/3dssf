# Upsampling

The task was to upsample a depth/disparity image with guided bilateral filtering.

## Input images

| RGB image | Quarter size depth image |
|-----------|-----------------------|
| <img src="input/im2.png" alt="RGB input image"/> | <img src="input/disp2.png" width="112" alt="Depth image"/> |


## Results

| Range Sigma \ Spatial Sigma | 2 | 4 | 8 | 16 |
|-------|---|---|---|---|
| 10.0  | <img src="output/ss0_sr0_output.png" width="200"/><img src="output/ss0_sr0_upsampled.png" width="200"/> | <img src="output/ss1_sr0_output.png" width="200"/><img src="output/ss1_sr0_upsampled.png" width="200"/> | <img src="output/ss2_sr0_output.png" width="200"/><img src="output/ss2_sr0_upsampled.png" width="200"/> | <img src="output/ss3_sr0_output.png" width="200"/><img src="output/ss3_sr0_upsampled.png" width="200"/> |
| 30.0  | <img src="output/ss0_sr1_output.png" width="200"/><img src="output/ss0_sr1_upsampled.png" width="200"/> | <img src="output/ss1_sr1_output.png" width="200"/><img src="output/ss1_sr1_upsampled.png" width="200"/> | <img src="output/ss2_sr1_output.png" width="200"/><img src="output/ss2_sr1_upsampled.png" width="200"/> | <img src="output/ss3_sr1_output.png" width="200"/><img src="output/ss3_sr1_upsampled.png" width="200"/> |
| 70.0  | <img src="output/ss0_sr2_output.png" width="200"/><img src="output/ss0_sr2_upsampled.png" width="200"/> | <img src="output/ss1_sr2_output.png" width="200"/><img src="output/ss1_sr2_upsampled.png" width="200"/> | <img src="output/ss2_sr2_output.png" width="200"/><img src="output/ss2_sr2_upsampled.png" width="200"/> | <img src="output/ss3_sr2_output.png" width="200"/><img src="output/ss3_sr2_upsampled.png" width="200"/> |
| 150.0 | <img src="output/ss0_sr3_output.png" width="200"/><img src="output/ss0_sr3_upsampled.png" width="200"/> | <img src="output/ss1_sr3_output.png" width="200"/><img src="output/ss1_sr3_upsampled.png" width="200"/> | <img src="output/ss2_sr3_output.png" width="200"/><img src="output/ss2_sr3_upsampled.png" width="200"/> | <img src="output/ss3_sr3_output.png" width="200"/><img src="output/ss3_sr3_upsampled.png" width="200"/> |