#include <iostream>
#include <chrono>
#include <opencv2/opencv.hpp>
#include "main.h"

// Start with the following arguments: input/im2.png input/disp2.png
int main(int argc, char** argv) {

    if (argc != 3) {
        std::cerr << "Usage: " << argv[0] << " IMAGE DISPARITY_IMAGE" << std::endl;
        return 1;
    }

    cv::Mat image = cv::imread(argv[1]);
    if (!image.data) {
        std::cerr << "No image data" << std::endl;
        return EXIT_FAILURE;
    }
    cv::Mat dispImage = cv::imread(argv[2]);
    if (!dispImage.data) {
        std::cerr << "No disparity image data" << std::endl;
        return EXIT_FAILURE;
    }

    int diameter = 9; // filter size, window size
    double sigmaS[] = { 2, 4, 8, 16 };
    double sigmaR[] = { 10.0, 30.0, 70.0, 150.0 };

    // Calculate upsampling factor
    double heightRatio = static_cast<double>(image.rows) / dispImage.rows;
    double widthRatio = static_cast<double>(image.cols) / dispImage.cols;
    double ratio = heightRatio > widthRatio ? widthRatio : heightRatio;
    int uf = static_cast<int>(log2(ratio));

    auto startTime = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            double ss = sigmaS[i];
            double sr = sigmaR[j];
            auto roundStart = std::chrono::high_resolution_clock::now();

            cv::Mat output = image.clone();
            std::cout << "Upsampling with SS " << ss << " and SR " << sr << " ... ";

            cv::Mat dispUpsampled = upsample(image, output, dispImage, uf, diameter, ss, sr);

            std::string prefix = "output/ss" + std::to_string(i) + "_sr" + std::to_string(j);
            std::string filename = prefix + "_output.png";
            cv::imwrite(filename, output);
            filename = prefix + "_upsampled.png";
            cv::imwrite(filename, dispUpsampled);
            auto timestamp = std::chrono::high_resolution_clock::now();
            auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(timestamp - roundStart);
            std::cout << "done in " << elapsed.count() / 1000.0 << " seconds" <<  std::endl;
        }
    }
    auto timestamp = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(timestamp - startTime);
    std::cout << "Finished in " << elapsed.count() / 1000.0 << " seconds" <<  std::endl;

//    cv::imshow("Image1", image1);
//    cv::waitKey();

    return 0;
}

double distance(int x1, int y1, int x2, int y2) {
    return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

double gaussian(double x, double sigma) {
    return exp(- (x * x) / (2 * sigma * sigma)) / (CV_2PI * sigma * sigma);
}

cv::Vec3f gaussian(cv::Vec3f x, double sigma) {
    cv::Vec3f out;
    out[0] = gaussian(x[0], sigma);
    out[1] = gaussian(x[1], sigma);
    out[2] = gaussian(x[2], sigma);
    return out;
}

cv::Mat upsample(cv::Mat &src, cv::Mat &dst, cv::Mat &dispSrc,
              int uf, int d, double sigmaS, double sigmaR) {
    cv::Mat dispUpsampled = dispSrc.clone();
    for (int i = 1; i <= uf - 1; i++) {
        cv::resize(dispUpsampled, dispUpsampled, cv::Size(), 2, 2);
        cv::Mat srcResized;
        cv::resize(src, srcResized, dispUpsampled.size());
        cv::Mat srcResizedDst = srcResized.clone();
        cv::Mat dispUpsampledDst = dispUpsampled.clone();
        jointBilateralFilter(srcResized, srcResizedDst, dispUpsampled, dispUpsampledDst, d, sigmaS, sigmaR);
        dispUpsampled = dispUpsampledDst;
    }
    cv::resize(dispUpsampled, dispUpsampled, src.size());

    cv::Mat dispDst = dispUpsampled.clone();
    jointBilateralFilter(src, dst, dispUpsampled, dispDst, d, sigmaS, sigmaR);
    return dispDst;
}

void jointBilateralFilter(cv::Mat &src, cv::Mat &dst,
                          cv::Mat &dispSrc, cv::Mat &dispDst,
                          int d, double sigmaS, double sigmaR) {
    int dHalf = d / 2;
    for (int i = dHalf; i < src.rows - dHalf; i++) {
        for (int j = dHalf; j < src.cols - dHalf; j++) {

            cv::Vec3b point = src.at<cv::Vec3b>(i, j);
            cv::Vec3b dispPoint = dispSrc.at<cv::Vec3b>(i, j);

            cv::Vec3f filteredPoint;
            cv::Vec3f filteredDispPoint;
            cv::Vec3f weightP;

            for (int k = -dHalf; k < dHalf; k++) {
                for (int l = -dHalf; l < dHalf; l++) {

                    cv::Vec3b neighbour = src.at<cv::Vec3b>(i + k, j + l);
                    cv::Vec3b dispNeighbour = dispSrc.at<cv::Vec3b>(i + k, j + l);

                    double gaussianSpatial = gaussian(distance(i, j, i + k, j + l), sigmaS);

                    cv::Vec3f diff;
                    cv::absdiff(point, neighbour, diff);
                    cv::Vec3f gaussianRange = gaussian(diff, sigmaR);

                    cv::Vec3f weight = gaussianRange * gaussianSpatial;

                    filteredPoint[0] += neighbour[0] * weight[0];
                    filteredPoint[1] += neighbour[1] * weight[1];
                    filteredPoint[2] += neighbour[2] * weight[2];

                    filteredDispPoint[0] += dispNeighbour[0] * weight[0];
                    filteredDispPoint[1] += dispNeighbour[1] * weight[1];
                    filteredDispPoint[2] += dispNeighbour[2] * weight[2];

                    cv::add(weightP, weight, weightP);
                }
            }
            filteredPoint[0] /= weightP[0];
            filteredPoint[1] /= weightP[1];
            filteredPoint[2] /= weightP[2];

            filteredDispPoint[0] /= weightP[0];
            filteredDispPoint[1] /= weightP[1];
            filteredDispPoint[2] /= weightP[2];

            dst.at<cv::Vec3b>(i, j) = filteredPoint;
            dispDst.at<cv::Vec3b>(i, j) = filteredDispPoint;
        }
    }
}

