# ICP and Trimmed ICP implementations

## Result samples

ICP for distorted highly overlapping point clouds

<img src="output/icp_results.png" />

---

Tr-ICP for distorted point clouds with low overlap

<img src="output/tr-icp_results.png" />

## Performance Diagrams

<img src="output/diagrams.png" />
