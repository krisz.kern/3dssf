#pragma once

void parsePLY(const std::string &filename, Eigen::MatrixX3d &mat);

void writePLY(const std::string &filename, const Eigen::MatrixX3d &mat);

int distortPointCloud(const std::string &filename, const std::string &suffix, Eigen::MatrixX3d &mat, int level);

void findClosestPoints(const nanoflann::KDTreeEigenMatrixAdaptor<Eigen::MatrixX3d> &kdTree,
                       const Eigen::MatrixX3d &points,
                       std::vector<Eigen::Index> &pointCorrespondences,
                       std::vector<double> &distances);

void icp(const std::string &input1, const std::string &input2, int distortionLevel);

void trimmedIcp(const std::string &input1, const std::string &input2, int distortionLevel);