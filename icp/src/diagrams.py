import matplotlib.pyplot as plt


def read_file(file_path):
    with open(file_path) as f:
        content = f.readlines()
    _mse = [float(line.strip().split()[0]) for line in content]
    _elapsed_ms = [int(line.strip().split()[1]) for line in content]
    return _mse, _elapsed_ms


fig, axs = plt.subplots(2, figsize=(20, 10))


axs[0].set_title('ICP')
axs[1].set_title('Tr-ICP')

for i in range(1, 6):
    filepath_1 = 'output/icp_distortion_' + str(i) + '.txt'
    filepath_2 = 'output/tr-icp_distortion_' + str(i) + '.txt'

    mse, elapsed_ms = read_file(filepath_1)
    axs[0].plot(elapsed_ms, mse, marker='o', label='distortion_' + str(i))

    mse, elapsed_ms = read_file(filepath_2)
    axs[1].plot(elapsed_ms, mse, marker='o', label='distortion_' + str(i))

axs[0].set(xlabel='Elapsed Time (ms)', ylabel='MSE - mean squared error')
axs[0].legend()
axs[0].set_ylim([0, 1])
axs[0].axhline(y=0.02, color='r', linestyle='--')
axs[1].set(xlabel='Elapsed Time (ms)', ylabel='MSE - mean squared error')
axs[1].legend()
axs[1].set_ylim([0, 1])
axs[1].axhline(y=0.02, color='r', linestyle='--')

plt.savefig('output/diagrams.png')
plt.show()
