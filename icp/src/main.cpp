#define _USE_MATH_DEFINES

#include <cmath>
#include <iostream>
#include <random>
#include <chrono>
#include <numeric>
#include <algorithm>

#include <Eigen/Dense>
#include "../include/nanoflann.hpp"
#include "../include/happly.h"
#include "main.h"

#define ITERATIONS 100
#define MSE_LIMIT 0.02
#define TR_ICP_RATIO 0.6

int main() {
    select_mode:
    std::cout << "Select mode:" << std::endl;
    std::cout << "1 - Standard ICP" << std::endl;
    std::cout << "2 - Trimmed ICP" << std::endl;
    std::cout << "3 - Run all" << std::endl;

    int mode;
    std::cin >> mode;

    std::string fountainA_file = "input/fountain_a.ply";
    std::string fountainB_file = "input/fountain_b.ply";

    switch (mode) {
        case 1:
            icp(fountainA_file, fountainA_file, -1);
            break;
        case 2:
            trimmedIcp(fountainA_file, fountainB_file, -1);
            break;
        case 3:
            std::cout << "--- Running all combinations ---" << std::endl;
            for (int i = 1; i <= 5; i++) {
                std::cout << "--- Distortion level " << i << " ---" << std::endl;
                icp(fountainA_file, fountainA_file, i);
                trimmedIcp(fountainA_file, fountainB_file, i);
            }
            break;
        default:
            std::cerr << mode << " is not valid" << std::endl << std::endl;
            goto select_mode;
    }

    return 0;
}

void parsePLY(const std::string &filename, Eigen::MatrixX3d &mat) {
    std::cout << "Parsing " << filename << " ... ";

    happly::PLYData plyIn(filename);

    std::vector<double> xPos = plyIn.getElement("vertex").getProperty<double>("x");
    std::vector<double> yPos = plyIn.getElement("vertex").getProperty<double>("y");
    std::vector<double> zPos = plyIn.getElement("vertex").getProperty<double>("z");

    mat.resize(xPos.size(), Eigen::NoChange);

    for (size_t i = 0; i < xPos.size(); ++i) {
        mat.row(i) = Eigen::Vector3d(xPos[i], yPos[i], zPos[i]);
    }

    std::cout << "done." << std::endl;
}

void writePLY(const std::string &filename, const Eigen::MatrixX3d &mat) {
    std::cout << "Writing " << filename << " ... ";

    std::vector<std::array<double, 3>> vertices;

    for (size_t i = 0; i < mat.rows(); ++i) {
        Eigen::Vector3d row = mat.row(i);
        vertices.emplace_back(std::array<double, 3>{{row.x(), row.y(), row.z()}});
    }

    happly::PLYData plyOut;

    plyOut.addVertexPositions(vertices);

    plyOut.write(filename, happly::DataFormat::ASCII);

    std::cout << "done." << std::endl;
}

std::string getOutputFilename(const std::string &input, const std::string &suffix) {
    int slashIdx = 0;
    int dotIdx = 0;
    for (int i = 0; i < input.size(); i++) {
        if (input[i] == '/') {
            slashIdx = i;
        } else if (input[i] == '.') {
            dotIdx = i;
        }
    }
    return "output" + input.substr(slashIdx, dotIdx - slashIdx) + suffix + input.substr(dotIdx);
}

int distortPointCloud(const std::string &filename, const std::string &suffix, Eigen::MatrixX3d &mat, int level) {
    int distortionLevel;
    if (level < 0) {
        std::cout << "Select distortion level [1-5]:";

        std::cin >> distortionLevel;
        std::cout << "Distorting point cloud (level " << distortionLevel << ") ... ";
    } else {
        distortionLevel = level;
    }

    std::random_device rd;
    std::mt19937 mt(rd());

    std::uniform_real_distribution<double> angleDist(-10, std::nextafter(10, DBL_MAX));

    Eigen::AngleAxisd Rx(angleDist(mt) * distortionLevel * M_PI / 180, Eigen::Vector3d::UnitX());
    Eigen::AngleAxisd Ry(angleDist(mt) * distortionLevel * M_PI / 180, Eigen::Vector3d::UnitY());
    Eigen::AngleAxisd Rz(angleDist(mt) * distortionLevel * M_PI / 180, Eigen::Vector3d::UnitZ());

    Eigen::Matrix3d R;
    R = Rx * Ry * Rz;

    mat *= R;

    std::normal_distribution<double> dispDist(0.0, 0.05 + 0.1 * (distortionLevel - 1));

    mat.unaryExpr([&dispDist, &mt](double x) { return x + dispDist(mt); });

    std::cout << "done." << std::endl;

    writePLY(getOutputFilename(filename, "_distorted_" + suffix + std::to_string(distortionLevel)), mat);
    return distortionLevel;
}

void findClosestPoints(const nanoflann::KDTreeEigenMatrixAdaptor<Eigen::MatrixX3d> &kdTree,
                       const Eigen::MatrixX3d &points,
                       std::vector<Eigen::Index> &pointCorrespondences,
                       std::vector<double> &distances) {
    std::cout << "Finding closest points ... ";

    distances.clear();
    pointCorrespondences.clear();

    nanoflann::KNNResultSet<double> resultSet(1);

    auto *indices = new size_t[1];
    auto *dists = new double[1];
    for (int rowIdx = 0; rowIdx < points.rows(); ++rowIdx) {
        resultSet.init(indices, dists);

        const double point[3] = {
                points(rowIdx, 0),
                points(rowIdx, 1),
                points(rowIdx, 2)
        };

        kdTree.index->findNeighbors(resultSet, point, nanoflann::SearchParams());

        pointCorrespondences.emplace_back(indices[0]);
        distances.emplace_back(dists[0]);
    }

    delete[] indices;
    delete[] dists;

    std::cout << "done." << std::endl;
}

void icp(const std::string &input1, const std::string &input2, int distortionLevel) {
    Eigen::MatrixX3d source;
    Eigen::MatrixX3d target;
    parsePLY(input1, source);
    parsePLY(input2, target);

    distortionLevel = distortPointCloud(input1, "icp", source, distortionLevel);

    std::cout << "Building KD Tree ... ";
    nanoflann::KDTreeEigenMatrixAdaptor<Eigen::MatrixX3d> kdTree(3, target);
    kdTree.index->buildIndex();
    std::cout << "done." << std::endl;

    std::cout << "Starting ICP" << std::endl;
    auto startTime = std::chrono::high_resolution_clock::now();

    std::vector<Eigen::Index> pointCorrespondences;
    std::vector<double> distances;
    const int N = source.rows();

    std::ofstream datafile;
    datafile.open("output/icp_distortion_" + std::to_string(distortionLevel) + ".txt");

    for (int i = 0; i < ITERATIONS; ++i) {
        findClosestPoints(kdTree, source, pointCorrespondences, distances);
        double mse = 0.0;
        for (double distance : distances) {
            mse += distance;
        }
        mse /= distances.size();
        std::cout << "MSE at iter " << i << ": " << mse << std::endl;
        if (mse < MSE_LIMIT) {
            std::cout << "MSE threshold reached, stopping ICP" << std::endl;
            break;
        }
        // Find centroids
        Eigen::Vector3d sourceCentroid, targetCentroid;
        Eigen::Matrix3Xd sourceTmp(3, source.rows());
        Eigen::Matrix3Xd targetTmp(3, source.rows());
        for (int j = 0; j < N; ++j) {
            Eigen::Vector3d sp = source.row(j);
            Eigen::Vector3d tp = target.row(pointCorrespondences[j]);
            sourceTmp.col(j) = sp.transpose();
            targetTmp.col(j) = tp.transpose();
            sourceCentroid += sp;
            targetCentroid += tp;
        }
        sourceCentroid /= N;
        targetCentroid /= N;

        sourceTmp = sourceTmp.colwise() - sourceCentroid;
        targetTmp = targetTmp.colwise() - targetCentroid;

        // Rotation
        Eigen::MatrixX3d H = sourceTmp * targetTmp.transpose();
        Eigen::JacobiSVD svd(H, Eigen::ComputeFullU | Eigen::ComputeFullV);
        Eigen::Matrix3d U = svd.matrixU();
        Eigen::Matrix3d V = svd.matrixV();
        Eigen::Matrix3d R = V * U.transpose();

        if (R.determinant() < 0) {
            V.col(2) *= -1;
            R = V * U.transpose();
        }

        // Translation
        Eigen::Vector3d t = targetCentroid - R * sourceCentroid;

        // Transformation
        source = ((R * source.transpose()).colwise() + t).transpose();
        auto timestamp = std::chrono::high_resolution_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(timestamp - startTime);
        datafile << mse << " " << elapsed.count() << std::endl;
    }
    datafile.close();

    writePLY("output/icp_distortion_" + std::to_string(distortionLevel) + ".ply", source);

    auto stopTime = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(stopTime - startTime);
    std::cout << "Finished ICP, elapsed time: " << elapsed.count() << " ms" << std::endl;
}

void trimmedIcp(const std::string &input1, const std::string &input2, int distortionLevel) {
    Eigen::MatrixX3d source;
    Eigen::MatrixX3d target;
    parsePLY(input1, source);
    parsePLY(input2, target);

    distortionLevel = distortPointCloud(input1, "tr-icp", source, distortionLevel);

    std::cout << "Building KD Tree ... ";
    nanoflann::KDTreeEigenMatrixAdaptor<Eigen::MatrixX3d> kdTree(3, target);
    kdTree.index->buildIndex();
    std::cout << "done." << std::endl;

    std::cout << "Starting Tr-ICP" << std::endl;
    auto startTime = std::chrono::high_resolution_clock::now();

    std::vector<Eigen::Index> pointCorrespondences;
    std::vector<double> distances;
    const int N = source.rows();

    const int maxMatches = (int) (N * TR_ICP_RATIO);

    std::ofstream datafile;
    datafile.open("output/tr-icp_distortion_" + std::to_string(distortionLevel) + ".txt");

    for (int i = 0; i < ITERATIONS; ++i) {
        findClosestPoints(kdTree, source, pointCorrespondences, distances);

        // https://stackoverflow.com/questions/1577475/c-sorting-and-keeping-track-of-indexes
        std::vector<size_t> idx(distances.size());
        std::iota(idx.begin(), idx.end(), 0);
        std::stable_sort(idx.begin(), idx.end(),
                         [&distances](size_t i1, size_t i2) { return distances[i1] < distances[i2]; });

        double mse = 0.0;
        for (int j = 0; j < maxMatches; ++j) {
            mse += distances[idx[j]];
        }
        mse /= maxMatches;
        std::cout << "MSE at iter " << i << ": " << mse << std::endl;
        if (mse < MSE_LIMIT) {
            std::cout << "MSE threshold reached, stopping ICP" << std::endl;
            break;
        }

        // Find centroids
        Eigen::Vector3d sourceCentroid, targetCentroid;
        Eigen::Matrix3Xd sourceTmp(3, maxMatches);
        Eigen::Matrix3Xd targetTmp(3, maxMatches);
        for (int j = 0; j < maxMatches; ++j) {
            Eigen::Vector3d sp = source.row(idx[j]);
            Eigen::Vector3d tp = target.row(pointCorrespondences[idx[j]]);
            sourceTmp.col(j) = sp.transpose();
            targetTmp.col(j) = tp.transpose();
            sourceCentroid += sp;
            targetCentroid += tp;
        }
        sourceCentroid /= N;
        targetCentroid /= N;

        sourceTmp = sourceTmp.colwise() - sourceCentroid;
        targetTmp = targetTmp.colwise() - targetCentroid;

        // Rotation
        Eigen::MatrixX3d H = sourceTmp * targetTmp.transpose();
        Eigen::JacobiSVD svd(H, Eigen::ComputeFullU | Eigen::ComputeFullV);
        Eigen::Matrix3d U = svd.matrixU();
        Eigen::Matrix3d V = svd.matrixV();
        Eigen::Matrix3d R = V * U.transpose();

        if (R.determinant() < 0) {
            V.col(2) *= -1;
            R = V * U.transpose();
        }

        // Translation
        Eigen::Vector3d t = targetCentroid - R * sourceCentroid;

        // Transformation
        source = ((R * source.transpose()).colwise() + t).transpose();

        auto timestamp = std::chrono::high_resolution_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(timestamp - startTime);
        datafile << mse << " " << elapsed.count() << std::endl;
    }
    datafile.close();

    writePLY("output/tr-icp_distortion_" + std::to_string(distortionLevel) + ".ply", source);

    auto stopTime = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(stopTime - startTime);
    std::cout << "Finished Tr-ICP, elapsed time: " << elapsed.count() << " ms" << std::endl;
}
